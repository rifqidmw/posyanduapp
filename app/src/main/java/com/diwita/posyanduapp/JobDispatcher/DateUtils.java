package com.diwita.posyanduapp.JobDispatcher;

import com.diwita.posyanduapp.model.DaftarAnakModel;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.util.ArrayList;
import java.util.Calendar;

public class DateUtils {

    private int year;
    private int month;
    private int day;

    public String setCurrentDateOnView() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        StringBuilder displayStringBuilder = new StringBuilder().append(month + 1).append("-").append(day).append("-").append(year).append(" ");
        return displayStringBuilder.toString();
    }

    public String[] convertStringToArray(String stringToConvert){
        String[] newStringArray = stringToConvert.split("-");
        return newStringArray;
    }

    public DateTime convertToDateTime(String stringToConvert) {
        String[] newStringArray = convertStringToArray(stringToConvert);
        int year = Integer.parseInt(newStringArray[2].trim());
        int day = Integer.parseInt(newStringArray[1].trim());
        int month = Integer.parseInt(newStringArray[0].trim());
        LocalDate mLocalDate = new LocalDate(year, month, day);
        DateTime firstDateTime = mLocalDate.toDateTime(LocalTime.fromDateFields(mLocalDate.toDate()));
        return firstDateTime;
    }

    public void displayAgeAnalysis(ArrayList<DaftarAnakModel> daftarAnak){
        for (int i=0; i<= daftarAnak.size(); i++){

        }
//        Period dateDifferencePeriod = displayBirthdayResult(dateToday, birthdayDate);
//        int getDateInMonths = dateDifferencePeriod.getMonths();
//        int getDateInYears = dateDifferencePeriod.getYears();
//        int mMonth = getDateInMonths + (getDateInYears * 12);
//
//        Toast.makeText(this, mMonth, Toast.LENGTH_SHORT).show();
//        totalDayResult.setText(Html.fromHtml(String.valueOf(mDay)));
//        totalWeekResult.setText(Html.fromHtml(String.valueOf(getDateInWeeks)));
//        totalMonthResult.setText(Html.fromHtml(String.valueOf(mMonth)));
//        totalYearResult.setText(Html.fromHtml(String.valueOf(getDateInYears)));
//        totalHourResult.setText(Html.fromHtml(String.valueOf(hours)));
//        totalMinuteResult.setText(Html.fromHtml(String.valueOf(minutes)));
//        totalSecondResult.setText(Html.fromHtml(String.valueOf(seconds)));
    }
    public Period displayBirthdayResult(DateTime dateToday, DateTime birthdayDate){
        Period dateDifferencePeriod = new Period(birthdayDate, dateToday, PeriodType.yearMonthDayTime());
        return dateDifferencePeriod;
    }
}
