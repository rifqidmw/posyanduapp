package com.diwita.posyanduapp.JobDispatcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class JobDispatcherReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("App", "called receiver method");
        try{
            AlarmUtils.generateNotification(context);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
