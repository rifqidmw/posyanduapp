package com.diwita.posyanduapp.JobDispatcher;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.SQLiteDatabaseHandler;
import com.diwita.posyanduapp.activities.DaftarAnak;
import com.diwita.posyanduapp.activities.DetailNotif;
import com.diwita.posyanduapp.adapters.NotifAdapter;
import com.diwita.posyanduapp.model.DaftarAnakModel;
import com.diwita.posyanduapp.model.DaftarListNotifikasi;
import com.diwita.posyanduapp.model.ResponseNotificationModel;
import com.diwita.posyanduapp.model.ValueNotificationModel;
import com.diwita.posyanduapp.retrofit.BaseApiService;
import com.diwita.posyanduapp.retrofit.UtilsApi;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.simplymadeapps.quickperiodicjobscheduler.PeriodicJob;
import com.simplymadeapps.quickperiodicjobscheduler.QuickJobFinishedCallback;
import com.simplymadeapps.quickperiodicjobscheduler.QuickPeriodicJob;
import com.simplymadeapps.quickperiodicjobscheduler.QuickPeriodicJobCollection;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobScheduling extends Application {
    private static Context context, mContext;


    @Override
    public void onCreate() {
        super.onCreate();
        JobScheduling.context = getApplicationContext();
        initJobsDay();
        initJobsWeek();
        initJobsMonth();
    }

    public void initJobsDay() {
        int jobId = 1;
        QuickPeriodicJob job = new QuickPeriodicJob(jobId, new PeriodicJob() {
            @Override
            public void execute(QuickJobFinishedCallback callback) {
                DayJobClass.performJob();

                callback.jobFinished();
            }
        });

        QuickPeriodicJobCollection.addJob(job);
    }

    public void initJobsWeek() {
        int jobId = 2;
        QuickPeriodicJob job = new QuickPeriodicJob(jobId, new PeriodicJob() {
            @Override
            public void execute(QuickJobFinishedCallback callback) {
                WeekJobClass.performJob();

                callback.jobFinished();
            }
        });

        QuickPeriodicJobCollection.addJob(job);
    }

    public void initJobsMonth() {
        int jobId = 3;
        QuickPeriodicJob job = new QuickPeriodicJob(jobId, new PeriodicJob() {
            @Override
            public void execute(QuickJobFinishedCallback callback) {
                MonthJobClass.performJob();

                callback.jobFinished();
            }
        });

        QuickPeriodicJobCollection.addJob(job);
    }
    public static Context getAppContext() {
        return JobScheduling.context;
    }

    public static class DayJobClass {

        String today="";
        String birthday="";

        public static void performJob() {

            final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("daftar anak").getRef();

            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    final String token = instanceIdResult.getToken();
                    if (token.isEmpty()){
                        Toast.makeText(context, "Token not generated...", Toast.LENGTH_SHORT).show();
                    }else {
//                        Toast.makeText(context, token, Toast.LENGTH_SHORT).show();

                        Query query = mDatabase.orderByChild("token").equalTo(token);
                        query.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (final DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                                    final DaftarAnakModel daftarAnakModel = dataSnapshot1.getValue(DaftarAnakModel.class);
                                    daftarAnakModel.setKey(dataSnapshot1.getKey());

                                    DateUtils dateUtils = new DateUtils();
                                    BaseApiService mApiService;
                                    mApiService = UtilsApi.getAPIService();
                                    String birthday = daftarAnakModel.getTgl();
                                    String[] items1 = birthday.split("-");
                                    String d1=items1[0];
                                    String m1=items1[1];
                                    String y1=items1[2];
                                    int d = Integer.parseInt(d1);
                                    int m = Integer.parseInt(m1);
                                    int y = Integer.parseInt(y1);

                                    Calendar today = new GregorianCalendar();
                                    today.setTime(new Date());
                                    DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MM-yyyy");
                                    DateTime dateTime = dtf.parseDateTime(birthday);
                                    LocalDate localDateBirtday = dateTime.toLocalDate();
                                    LocalDate localDate = new LocalDate();

                                    //month
                                    Months months = Months.monthsBetween(localDateBirtday, localDate);
                                    int ageInMonths = months.getMonths();

                                    //week
                                    Weeks weeks = Weeks.weeksBetween(localDateBirtday, localDate);
                                    int ageInWeeks = weeks.getWeeks();

                                    //day
                                    Days days = Days.daysBetween(localDateBirtday, localDate);
                                    final int ageInDays = days.getDays();

                                    Log.d("day", Integer.toString(ageInDays));
                                    Log.d("KEY", dataSnapshot1.getKey());

                                    if (ageInDays == 1){
                                        ValueNotificationModel valueNotificationModel = new ValueNotificationModel();
                                        valueNotificationModel.setTitle(daftarAnakModel.getNama_anak());
                                        valueNotificationModel.setMessage("Imunisasi Hepatitis");
                                        valueNotificationModel.setToken(token);
                                        mApiService.sendNotification(valueNotificationModel)
                                                .enqueue(new Callback<ResponseNotificationModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseNotificationModel> call, Response<ResponseNotificationModel> response) {
                                                        Log.d("Notifcation", response.message());
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseNotificationModel> call, Throwable t) {
                                                        Log.d("Error", t.getMessage());
                                                    }
                                                });
                                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("list notifikasi");

                                        DaftarListNotifikasi daftarListNotifikasi = new DaftarListNotifikasi();
                                        String id = mDatabase.push().getKey();
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        String email = user.getEmail();
                                        daftarListNotifikasi.setUser(email);
                                        daftarListNotifikasi.setNama(daftarAnakModel.getNama_anak());
                                        daftarListNotifikasi.setUmur(Integer.toString(ageInDays)+ " Hari");
                                        daftarListNotifikasi.setImunisasi("Imunisasi Hepatitis");
                                        daftarListNotifikasi.setToken(token);
                                        daftarListNotifikasi.setKey(dataSnapshot1.getKey());
                                        mDatabase.child(id).setValue(daftarListNotifikasi);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            });
        }
    }

    public static class WeekJobClass {

        String today="";
        String birthday="";

        public static void performJob() {

            final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("daftar anak").getRef();

            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    final String token = instanceIdResult.getToken();
                    if (token.isEmpty()){
                        Toast.makeText(context, "Token not generated...", Toast.LENGTH_SHORT).show();
                    }else {
//                        Toast.makeText(context, token, Toast.LENGTH_SHORT).show();

                        Query query = mDatabase.orderByChild("token").equalTo(token);
                        query.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                                    final DaftarAnakModel daftarAnakModel = dataSnapshot1.getValue(DaftarAnakModel.class);
                                    daftarAnakModel.setKey(dataSnapshot1.getKey());

                                    DateUtils dateUtils = new DateUtils();
                                    BaseApiService mApiService;
                                    mApiService = UtilsApi.getAPIService();
                                    String birthday = daftarAnakModel.getTgl();
                                    String[] items1 = birthday.split("-");
                                    String d1=items1[0];
                                    String m1=items1[1];
                                    String y1=items1[2];
                                    int d = Integer.parseInt(d1);
                                    int m = Integer.parseInt(m1);
                                    int y = Integer.parseInt(y1);

                                    Calendar today = new GregorianCalendar();
                                    today.setTime(new Date());
                                    DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MM-yyyy");
                                    DateTime dateTime = dtf.parseDateTime(birthday);
                                    LocalDate localDateBirtday = dateTime.toLocalDate();
                                    LocalDate localDate = new LocalDate();

                                    //month
                                    Months months = Months.monthsBetween(localDateBirtday, localDate);
                                    int ageInMonths = months.getMonths();

                                    //week
                                    Weeks weeks = Weeks.weeksBetween(localDateBirtday, localDate);
                                    final int ageInWeeks = weeks.getWeeks();

                                    //day
                                    Days days = Days.daysBetween(localDateBirtday, localDate);
                                    int ageInDays = days.getDays();

                                    Log.d("week", Integer.toString(ageInWeeks));

                                    if (ageInWeeks == 6){
                                        ValueNotificationModel valueNotificationModel = new ValueNotificationModel();
                                        valueNotificationModel.setTitle(daftarAnakModel.getNama_anak());
                                        valueNotificationModel.setMessage("Imunisasi DPT");
                                        valueNotificationModel.setToken(token);
                                        mApiService.sendNotification(valueNotificationModel)
                                                .enqueue(new Callback<ResponseNotificationModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseNotificationModel> call, Response<ResponseNotificationModel> response) {
                                                        Log.d("Notifcation", response.message());
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseNotificationModel> call, Throwable t) {
                                                        Log.d("Error", t.getMessage());
                                                    }
                                                });
                                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("list notifikasi");

                                        DaftarListNotifikasi daftarListNotifikasi = new DaftarListNotifikasi();
                                        String id = mDatabase.push().getKey();
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        String email = user.getEmail();
                                        daftarListNotifikasi.setUser(email);
                                        daftarListNotifikasi.setNama(daftarAnakModel.getNama_anak());
                                        daftarListNotifikasi.setUmur(Integer.toString(ageInWeeks)+ " Minggu");
                                        daftarListNotifikasi.setImunisasi("Imunisasi DPT");
                                        daftarListNotifikasi.setToken(token);
                                        daftarListNotifikasi.setKey(dataSnapshot1.getKey());
                                        mDatabase.child(id).setValue(daftarListNotifikasi);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            });
        }
    }

    public static class MonthJobClass {

        String today="";
        String birthday="";

        public static void performJob() {

            final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("daftar anak").getRef();

            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    final String token = instanceIdResult.getToken();
                    if (token.isEmpty()){
                        Toast.makeText(context, "Token not generated...", Toast.LENGTH_SHORT).show();
                    }else {
//                        Toast.makeText(context, token, Toast.LENGTH_SHORT).show();

                        Query query = mDatabase.orderByChild("token").equalTo(token);
                        query.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                                    final DaftarAnakModel daftarAnakModel = dataSnapshot1.getValue(DaftarAnakModel.class);
                                    daftarAnakModel.setKey(dataSnapshot1.getKey());

                                    DateUtils dateUtils = new DateUtils();
                                    BaseApiService mApiService;
                                    mApiService = UtilsApi.getAPIService();
                                    String birthday = daftarAnakModel.getTgl();
                                    String[] items1 = birthday.split("-");
                                    String d1=items1[0];
                                    String m1=items1[1];
                                    String y1=items1[2];
                                    int d = Integer.parseInt(d1);
                                    int m = Integer.parseInt(m1);
                                    int y = Integer.parseInt(y1);

                                    Calendar today = new GregorianCalendar();
                                    today.setTime(new Date());
                                    DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MM-yyyy");
                                    DateTime dateTime = dtf.parseDateTime(birthday);
                                    LocalDate localDateBirtday = dateTime.toLocalDate();
                                    LocalDate localDate = new LocalDate();

                                    //month
                                    Months months = Months.monthsBetween(localDateBirtday, localDate);
                                    final int ageInMonths = months.getMonths();

                                    //week
                                    Weeks weeks = Weeks.weeksBetween(localDateBirtday, localDate);
                                    int ageInWeeks = weeks.getWeeks();

                                    //day
                                    Days days = Days.daysBetween(localDateBirtday, localDate);
                                    int ageInDays = days.getDays();

                                    Log.d("month", Integer.toString(ageInMonths));


                                    if (ageInMonths == 2 || ageInMonths == 3 || ageInMonths == 4){
                                        ValueNotificationModel valueNotificationModel = new ValueNotificationModel();
                                        valueNotificationModel.setTitle(daftarAnakModel.getNama_anak());
                                        valueNotificationModel.setMessage("Imunisasi Hepatitis");
                                        valueNotificationModel.setToken(token);

                                        mApiService.sendNotification(valueNotificationModel)
                                                .enqueue(new Callback<ResponseNotificationModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseNotificationModel> call, Response<ResponseNotificationModel> response) {
                                                        Log.d("Notifcation", response.message());
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseNotificationModel> call, Throwable t) {
                                                        Log.d("Error", t.getMessage());
                                                    }
                                                });

                                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("list notifikasi");

                                        DaftarListNotifikasi daftarListNotifikasi = new DaftarListNotifikasi();
                                        String id = mDatabase.push().getKey();
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        String email = user.getEmail();
                                        daftarListNotifikasi.setUser(email);
                                        daftarListNotifikasi.setNama(daftarAnakModel.getNama_anak());
                                        daftarListNotifikasi.setUmur(Integer.toString(ageInMonths)+ " Bulan");
                                        daftarListNotifikasi.setImunisasi("Imunisasi Hepatitis");
                                        daftarListNotifikasi.setToken(token);
                                        daftarListNotifikasi.setKey(dataSnapshot1.getKey());
                                        mDatabase.child(id).setValue(daftarListNotifikasi);
                                    }
                                    if (ageInMonths == 2){
                                        ValueNotificationModel valueNotificationModel = new ValueNotificationModel();
                                        valueNotificationModel.setTitle(daftarAnakModel.getNama_anak());
                                        valueNotificationModel.setMessage("Imunisasi Polio");
                                        valueNotificationModel.setToken(token);
                                        mApiService.sendNotification(valueNotificationModel)
                                                .enqueue(new Callback<ResponseNotificationModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseNotificationModel> call, Response<ResponseNotificationModel> response) {
                                                        Log.d("Notifcation", response.message());
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseNotificationModel> call, Throwable t) {
                                                        Log.d("Error", t.getMessage());
                                                    }
                                                });

                                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("list notifikasi");

                                        DaftarListNotifikasi daftarListNotifikasi = new DaftarListNotifikasi();
                                        String id = mDatabase.push().getKey();
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        String email = user.getEmail();
                                        daftarListNotifikasi.setUser(email);
                                        daftarListNotifikasi.setNama(daftarAnakModel.getNama_anak());
                                        daftarListNotifikasi.setUmur(Integer.toString(ageInMonths)+ " Bulan");
                                        daftarListNotifikasi.setImunisasi("Imunisasi Polio");
                                        daftarListNotifikasi.setToken(token);
                                        daftarListNotifikasi.setKey(dataSnapshot1.getKey());
                                        mDatabase.child(id).setValue(daftarListNotifikasi);
                                    }
                                    if (ageInMonths == 2){
                                        ValueNotificationModel valueNotificationModel = new ValueNotificationModel();
                                        valueNotificationModel.setTitle(daftarAnakModel.getNama_anak());
                                        valueNotificationModel.setMessage("Imunisasi Hib");
                                        valueNotificationModel.setToken(token);
                                        mApiService.sendNotification(valueNotificationModel)
                                                .enqueue(new Callback<ResponseNotificationModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseNotificationModel> call, Response<ResponseNotificationModel> response) {
                                                        Log.d("Notifcation", response.message());
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseNotificationModel> call, Throwable t) {
                                                        Log.d("Error", t.getMessage());
                                                    }
                                                });
                                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("list notifikasi");

                                        DaftarListNotifikasi daftarListNotifikasi = new DaftarListNotifikasi();
                                        String id = mDatabase.push().getKey();
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        String email = user.getEmail();
                                        daftarListNotifikasi.setUser(email);
                                        daftarListNotifikasi.setNama(daftarAnakModel.getNama_anak());
                                        daftarListNotifikasi.setUmur(Integer.toString(ageInMonths)+ " Bulan");
                                        daftarListNotifikasi.setImunisasi("Imunisasi HIB");
                                        daftarListNotifikasi.setToken(token);
                                        daftarListNotifikasi.setKey(dataSnapshot1.getKey());
                                        mDatabase.child(id).setValue(daftarListNotifikasi);
                                    }
                                    if (ageInMonths == 3){
                                        ValueNotificationModel valueNotificationModel = new ValueNotificationModel();
                                        valueNotificationModel.setTitle(daftarAnakModel.getNama_anak());
                                        valueNotificationModel.setMessage("Imunisasi BCG");
                                        valueNotificationModel.setToken(token);
                                        mApiService.sendNotification(valueNotificationModel)
                                                .enqueue(new Callback<ResponseNotificationModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseNotificationModel> call, Response<ResponseNotificationModel> response) {
                                                        Log.d("Notifcation", response.message());
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseNotificationModel> call, Throwable t) {
                                                        Log.d("Error", t.getMessage());
                                                    }
                                                });
                                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("list notifikasi");

                                        DaftarListNotifikasi daftarListNotifikasi = new DaftarListNotifikasi();
                                        String id = mDatabase.push().getKey();
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        String email = user.getEmail();
                                        daftarListNotifikasi.setUser(email);
                                        daftarListNotifikasi.setNama(daftarAnakModel.getNama_anak());
                                        daftarListNotifikasi.setUmur(Integer.toString(ageInMonths)+ " Bulan");
                                        daftarListNotifikasi.setImunisasi("Imunisasi BCG");
                                        daftarListNotifikasi.setToken(token);
                                        daftarListNotifikasi.setKey(dataSnapshot1.getKey());
                                        mDatabase.child(id).setValue(daftarListNotifikasi);
                                    }
                                    if (ageInMonths == 9){
                                        ValueNotificationModel valueNotificationModel = new ValueNotificationModel();
                                        valueNotificationModel.setTitle(daftarAnakModel.getNama_anak());
                                        valueNotificationModel.setMessage("Imunisasi Campak");
                                        valueNotificationModel.setToken(token);
                                        mApiService.sendNotification(valueNotificationModel)
                                                .enqueue(new Callback<ResponseNotificationModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseNotificationModel> call, Response<ResponseNotificationModel> response) {
                                                        Log.d("Notifcation", response.message());
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseNotificationModel> call, Throwable t) {
                                                        Log.d("Error", t.getMessage());
                                                    }
                                                });
                                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("list notifikasi");

                                        DaftarListNotifikasi daftarListNotifikasi = new DaftarListNotifikasi();
                                        String id = mDatabase.push().getKey();
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        String email = user.getEmail();
                                        daftarListNotifikasi.setUser(email);
                                        daftarListNotifikasi.setNama(daftarAnakModel.getNama_anak());
                                        daftarListNotifikasi.setUmur(Integer.toString(ageInMonths)+ " Bulan");
                                        daftarListNotifikasi.setImunisasi("Imunisasi Campak");
                                        daftarListNotifikasi.setToken(token);
                                        daftarListNotifikasi.setKey(dataSnapshot1.getKey());
                                        mDatabase.child(id).setValue(daftarListNotifikasi);
                                    }
                                    if (ageInMonths == 12){
                                        ValueNotificationModel valueNotificationModel = new ValueNotificationModel();
                                        valueNotificationModel.setTitle(daftarAnakModel.getNama_anak());
                                        valueNotificationModel.setMessage("Imunisasi MMR");
                                        valueNotificationModel.setToken(token);
                                        mApiService.sendNotification(valueNotificationModel)
                                                .enqueue(new Callback<ResponseNotificationModel>() {
                                                    @Override
                                                    public void onResponse(Call<ResponseNotificationModel> call, Response<ResponseNotificationModel> response) {
                                                        Log.d("Notifcation", response.message());
                                                    }

                                                    @Override
                                                    public void onFailure(Call<ResponseNotificationModel> call, Throwable t) {
                                                        Log.d("Error", t.getMessage());
                                                    }
                                                });
                                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("list notifikasi");

                                        DaftarListNotifikasi daftarListNotifikasi = new DaftarListNotifikasi();
                                        String id = mDatabase.push().getKey();
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        String email = user.getEmail();
                                        daftarListNotifikasi.setUser(email);
                                        daftarListNotifikasi.setNama(daftarAnakModel.getNama_anak());
                                        daftarListNotifikasi.setUmur(Integer.toString(ageInMonths)+ " Bulan");
                                        daftarListNotifikasi.setImunisasi("Imunisasi MMR");
                                        daftarListNotifikasi.setToken(token);
                                        daftarListNotifikasi.setKey(dataSnapshot1.getKey());
                                        mDatabase.child(id).setValue(daftarListNotifikasi);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            });
        }
    }
}
