package com.diwita.posyanduapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.diwita.posyanduapp.model.DaftarListNotifikasi;

import java.util.ArrayList;
import java.util.List;

public class SQLiteDatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Posyandu123";
    private static final String TABLE_NAME = "detail_notifikasi";

    public SQLiteDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATION_TABLE = "CREATE TABLE jadwal_pengajaran ( "
                + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "judul TEXT, "
                + "nim TEXT, "
                + "nama TEXT, "
                + "tanggal TEXT, "
                + "waktu TEXT, "
                + "ruang TEXT, "
                + "narasumber1 TEXT, "
                + "narasumber2 TEXT, "
                + "narasumber3 TEXT)"
                ;

        db.execSQL(CREATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // you can implement here migration process
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        this.onCreate(db);
    }

    public void addJadwal(DaftarListNotifikasi jadwal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("nama", jadwal.getNama());
        values.put("umur", jadwal.getUmur());
        values.put("imunisasi", jadwal.getImunisasi());
        values.put("token", jadwal.getToken());
        // insert
        db.insert(TABLE_NAME,null, values);
        db.close();
    }


    public List<DaftarListNotifikasi> getJadwal(String token) {
        List<DaftarListNotifikasi> list = new ArrayList<>();
        String selectQuery = "SELECT  * FROM detail_notifikasi WHERE token='"+token+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                DaftarListNotifikasi jadwal = new DaftarListNotifikasi();
                jadwal.setToken(cursor.getString(1));
                jadwal.setNama(cursor.getString(2));
                jadwal.setUmur(cursor.getString(3));
                jadwal.setImunisasi(cursor.getString(4));
                list.add(jadwal);
            } while (cursor.moveToNext());
        }

        db.close();
        return list;
    }

}
