package com.diwita.posyanduapp.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.diwita.posyanduapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class DaftarActivity extends AppCompatActivity {
    private String user;
    private String pass;
    private TextView result;
    private Button btnDaftar;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etNama;
    private ProgressBar progressBar;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        FirebaseApp.initializeApp(this);
        auth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressBar);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etNama = findViewById(R.id.etNama);
        btnDaftar = findViewById(R.id.btnDaftar);
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = etNama.getText().toString();
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                daftar(nama, email, password);
            }
        });
    }

    private void daftar(String nama,String email, String password){
        if (TextUtils.isEmpty(nama)){
            etNama.setError("Nama harus diisi !!!");
            return;
        }
        if (TextUtils.isEmpty(email)){
            etEmail.setError("Email harus diisi !!!");
            return;
        }
        if (TextUtils.isEmpty(password)){
            etPassword.setError("Password harus diisi !!!");
            return;
        }
        if (password.length() < 6){
            etPassword.setError("Password minimal 6 karakter !!!");
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(DaftarActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (!task.isSuccessful()){
                            Toast.makeText(DaftarActivity.this, "Authentication gagal !!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DaftarActivity.this, "Daftar telah berhasil, silahkan login !!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(DaftarActivity.this, LoginActivity.class));
                            finish();
                        }
                    }
                });
    }


    @Override
    protected void onResume(){
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}
