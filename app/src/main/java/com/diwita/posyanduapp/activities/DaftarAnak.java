package com.diwita.posyanduapp.activities;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.model.DaftarAnakModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Calendar;

public class DaftarAnak extends AppCompatActivity {

    private ImageView btnDate;
    private EditText etNik;
    private EditText etNama;
    private EditText etDate;
    private EditText etBeratBadan;
    private EditText etTinggiBadan;
    private RadioGroup radioJK;
    private Button btnAdd;
    private DatePickerDialog datePickerDialog;
    private String jk = "";
    private String token = "";

    private PendingIntent pendingIntent;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_anak);
        etNik = findViewById(R.id.txtNik);
        etNama = findViewById(R.id.txtNamaAnak);
        btnDate = findViewById(R.id.btnDate);
        etDate = findViewById(R.id.etDate);
        radioJK = findViewById(R.id.radioJK);
        etBeratBadan = findViewById(R.id.txtBeratBadan);
        etTinggiBadan = findViewById(R.id.txtTinggiBadan);
        btnAdd = findViewById(R.id.btnAddAnak);

        mDatabase = FirebaseDatabase.getInstance().getReference("daftar anak");

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
                if (token.isEmpty()){
                    Toast.makeText(DaftarAnak.this, "Token not generated...", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(DaftarAnak.this, token, Toast.LENGTH_SHORT).show();
                }
            }
        });

        radioJK.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioLaki:
                        jk = "Laki-laki";
                        Toast.makeText(DaftarAnak.this,
                                jk, Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioWanita:
                        jk = "Perempuan";
                        Toast.makeText(DaftarAnak.this,
                                jk, Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(DaftarAnak.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                etDate.setText(dayOfMonth + "-"
                                        + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAnak();
            }
        });
    }

    private void addAnak(){

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2019);
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 10);
        calendar.set(Calendar.HOUR_OF_DAY, 17);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.AM_PM,Calendar.AM);

        String nik = etNik.getText().toString();
        String nama = etNama.getText().toString();
        String tgl = etDate.getText().toString();
        String bb = etBeratBadan.getText().toString();
        String tb = etTinggiBadan.getText().toString();
        String id = mDatabase.push().getKey();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String email = user.getEmail();

        final DaftarAnakModel daftarAnakModel = new DaftarAnakModel();
        daftarAnakModel.setNik(nik);
        daftarAnakModel.setNama_anak(nama);
        daftarAnakModel.setTgl(tgl);
        daftarAnakModel.setJk(jk);
        daftarAnakModel.setBerat_badan(bb);
        daftarAnakModel.setTinggi_badan(tb);
        daftarAnakModel.setToken(token);
        daftarAnakModel.setBbHepatitis1("");
        daftarAnakModel.setBbHepatitis2("");
        daftarAnakModel.setBbDpt("");
        daftarAnakModel.setBbPolio("");
        daftarAnakModel.setBbHib("");
        daftarAnakModel.setBbHepatitis3("");
        daftarAnakModel.setBbBcg("");
        daftarAnakModel.setBbHepatitis4("");
        daftarAnakModel.setBbCampak("");
        daftarAnakModel.setBbMmr("");
        daftarAnakModel.setUser(email);
        daftarAnakModel.setKey(id);

        if (token.isEmpty() || token.equals("")){
            Toast.makeText(this, "Gagal menyimpan data anak", Toast.LENGTH_SHORT).show();
        } else {
            mDatabase.child(id).setValue(daftarAnakModel);
            Toast.makeText(this, "Data Anak berhasil ditambahkan", Toast.LENGTH_LONG).show();
        }


        Intent myIntent = new Intent(DaftarAnak.this, MyReceiver.class);
        final int _id = (int) System.currentTimeMillis();
        pendingIntent = PendingIntent.getBroadcast(DaftarAnak.this, _id, myIntent,PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);

        etNama.setText("");
        etNik.setText("");
        etDate.setText("");
        etBeratBadan.setText("");
        etTinggiBadan.setText("");

        Intent intent = new Intent(DaftarAnak.this, MenuActivity.class);
        startActivity(intent);
    }

}
