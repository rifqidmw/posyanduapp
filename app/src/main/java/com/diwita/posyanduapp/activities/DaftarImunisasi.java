package com.diwita.posyanduapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.model.DaftarAnakModel;
import com.diwita.posyanduapp.model.DaftarListNotifikasi;

import java.util.ArrayList;

public class DaftarImunisasi extends AppCompatActivity implements View.OnClickListener {

    CheckBox bcg, hepatitis_b, polio, dpt, campak, hib, mmr;
    Button simpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_imunisasi);

        bcg = (CheckBox) findViewById(R.id.bcg);
        hepatitis_b = (CheckBox) findViewById(R.id.hepatitisb);
        polio = (CheckBox) findViewById(R.id.polio);
        dpt = (CheckBox) findViewById(R.id.dpt);
        campak = (CheckBox) findViewById(R.id.campak);
        hib = (CheckBox) findViewById(R.id.hib);
        mmr = (CheckBox) findViewById(R.id.mmr);
        simpan = (Button) findViewById(R.id.simpan);

        bcg.setOnClickListener(this);
        hepatitis_b.setOnClickListener(this);
        polio.setOnClickListener(this);
        dpt.setOnClickListener(this);
        campak.setOnClickListener(this);
        hib.setOnClickListener(this);
        mmr.setOnClickListener(this);
        simpan.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bcg:
                if (bcg.isChecked()) {
                    //logic
                }
                break;
            case  R.id.hepatitisb:
                if (hepatitis_b.isChecked()) {
                    //logic
                }
                break;
            case R.id.polio:
                if (polio.isChecked()) {
                    //logic
                }
                break;
            case R.id.dpt:
                if (dpt.isChecked()) {
                    //logic
                }
                break;
            case R.id.campak:
                if (campak.isChecked()) {
                    //logic
                }
                break;
            case R.id.hib:
                if (hib.isChecked()) {
                    //logic
                }
                break;
            case R.id.mmr:
                if (mmr.isChecked()) {
                    //logic
                }
                break;
        }
    //logic simpan
    }
}
