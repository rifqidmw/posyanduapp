package com.diwita.posyanduapp.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.SQLiteDatabaseHandler;
import com.diwita.posyanduapp.adapters.AnakAdapter;
import com.diwita.posyanduapp.adapters.NotifAdapter;
import com.diwita.posyanduapp.model.DaftarAnakModel;
import com.diwita.posyanduapp.model.DaftarListNotifikasi;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.List;

public class DetailNotif extends AppCompatActivity {

    private RecyclerView rvImunisasi;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<DaftarListNotifikasi> daftarNotifikasi = new ArrayList<>();

    private SQLiteDatabaseHandler db;
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_notif);

        rvImunisasi = (RecyclerView) findViewById(R.id.rvNotifikasi);

        rvImunisasi.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvImunisasi.setLayoutManager(layoutManager);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("list notifikasi").getRef();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                if (token.isEmpty()){
                    Toast.makeText(DetailNotif.this, "Token not generated...", Toast.LENGTH_SHORT).show();
                }else {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    String email = user.getEmail();
                    Query query = mDatabase.orderByChild("user").equalTo(email);
                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            daftarNotifikasi = new ArrayList<>();
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                                DaftarListNotifikasi daftarListNotifikasi = dataSnapshot1.getValue(DaftarListNotifikasi.class);
                                daftarNotifikasi.add(daftarListNotifikasi);
                            }
                            adapter = new NotifAdapter(daftarNotifikasi, DetailNotif.this);
                            rvImunisasi.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });
    }
}
