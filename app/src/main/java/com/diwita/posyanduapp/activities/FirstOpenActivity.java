package com.diwita.posyanduapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.firebase.FirebaseInstanceIDService;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class FirstOpenActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_open);

        mDatabase = FirebaseDatabase.getInstance().getReference("token");


        String key = "1";
        Boolean firstTime = getPreferences(MODE_PRIVATE).getBoolean(key, true);

        if (firstTime){
            getPreferences(MODE_PRIVATE).edit().putBoolean(key, false).apply();
            startActivity(new Intent(FirstOpenActivity.this, MainActivity.class));
            final String id = mDatabase.push().getKey();

            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String deviceToken = instanceIdResult.getToken();

                    mDatabase.child(id).setValue(deviceToken);
                }
            });
        }

    }
}
