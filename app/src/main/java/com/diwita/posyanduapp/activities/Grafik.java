package com.diwita.posyanduapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Line;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.MarkerType;
import com.anychart.enums.TooltipPositionMode;
import com.anychart.graphics.vector.Stroke;
import com.diwita.posyanduapp.R;

import java.util.ArrayList;
import java.util.List;

public class Grafik extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafik);
        AnyChartView anyChartView = findViewById(R.id.any_chart_view);
        anyChartView.setProgressBar(findViewById(R.id.progress_bar));

        Intent intent = getIntent();
        String nik = intent.getStringExtra("NIK");
        String nama = intent.getStringExtra("NAMA");
        String tgl_lahir = intent.getStringExtra("TGL_LAHIR");
        String token = intent.getStringExtra("TOKEN");
        String key = intent.getStringExtra("KEY");

        String bb = intent.getStringExtra("BB");
        String bb_hepatitis1 = intent.getStringExtra("BB_HEPATITIS1");
        String bb_hepatitis2 = intent.getStringExtra("BB_HEPATITIS2");
        String bb_dpt = intent.getStringExtra("BB_DPT");
        String bb_polio = intent.getStringExtra("BB_POLIO");
        String bb_hib = intent.getStringExtra("BB_HIB");
        String bb_hepatitis3 = intent.getStringExtra("BB_HEPATITIS3");
        String bb_bgc = intent.getStringExtra("BB_BGC");
        String bb_hepatitis4 = intent.getStringExtra("BB_HEPATITIS4");
        String bb_campak = intent.getStringExtra("BB_CAMPAK");
        String bb_mmr = intent.getStringExtra("BB_MMR");

        Cartesian cartesian = AnyChart.line();

        cartesian.animation(true);

        cartesian.padding(10d, 20d, 5d, 20d);

        cartesian.crosshair().enabled(true);
        cartesian.crosshair()
                .yLabel(true)
                .yStroke((Stroke) null, null, null, (String) null, (String) null);

        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);

        cartesian.title("Grafik Berat Badan Anak");

        cartesian.yAxis(0).title("Berat Badan (KG)");
        cartesian.xAxis(0).title("Umur").labels().padding(5d, 5d, 5d, 5d);

        List<DataEntry> seriesData = new ArrayList<>();
        seriesData.add(new CustomDataEntry("0 Hari", Integer.parseInt(bb)));
        if (!bb_hepatitis1.equals("")){
            seriesData.add(new CustomDataEntry("1 Hari", Integer.parseInt(bb_hepatitis1)));
        }
        if (!bb_hepatitis2.equals("")){
            seriesData.add(new CustomDataEntry("2 Bulan", Integer.parseInt(bb_hepatitis2)));
        }
        if (!bb_dpt.equals("")){
            seriesData.add(new CustomDataEntry("6 Minggu", Integer.parseInt(bb_dpt)));
        }
        if (!bb_polio.equals("")){
            seriesData.add(new CustomDataEntry("2 Bulan", Integer.parseInt(bb_polio)));
        }
        if (!bb_hib.equals("")){
            seriesData.add(new CustomDataEntry("2 Bulan", Integer.parseInt(bb_hib)));
        }
        if (!bb_hepatitis3.equals("")){
            seriesData.add(new CustomDataEntry("3 Bulan", Integer.parseInt(bb_hepatitis3)));
        }
        if (!bb_bgc.equals("")){
            seriesData.add(new CustomDataEntry("3 Bulan", Integer.parseInt(bb_bgc)));
        }
        if (!bb_hepatitis4.equals("")){
            seriesData.add(new CustomDataEntry("4 Bulan", Integer.parseInt(bb_hepatitis4)));
        }
        if (!bb_campak.equals("")){
            seriesData.add(new CustomDataEntry("9 Bulan", Integer.parseInt(bb_campak)));
        }
        if (!bb_mmr.equals("")){
            seriesData.add(new CustomDataEntry("12 Bulan", Integer.parseInt(bb_mmr)));
        }


        Set set = Set.instantiate();
        set.data(seriesData);
        Mapping series1Mapping = set.mapAs("{ x: 'x', value: 'value' }");
//        Mapping series2Mapping = set.mapAs("{ x: 'x', value: 'value2' }");
//        Mapping series3Mapping = set.mapAs("{ x: 'x', value: 'value3' }");

        Line series1 = cartesian.line(series1Mapping);
        series1.name("Berat Badan");
        series1.hovered().markers().enabled(true);
        series1.hovered().markers()
                .type(MarkerType.CIRCLE)
                .size(4d);
        series1.tooltip()
                .position("right")
                .anchor(Anchor.LEFT_CENTER)
                .offsetX(5d)
                .offsetY(5d);

//        Line series2 = cartesian.line(series2Mapping);
//        series2.name("Whiskey");
//        series2.hovered().markers().enabled(true);
//        series2.hovered().markers()
//                .type(MarkerType.CIRCLE)
//                .size(4d);
//        series2.tooltip()
//                .position("right")
//                .anchor(Anchor.LEFT_CENTER)
//                .offsetX(5d)
//                .offsetY(5d);
//
//        Line series3 = cartesian.line(series3Mapping);
//        series3.name("Tequila");
//        series3.hovered().markers().enabled(true);
//        series3.hovered().markers()
//                .type(MarkerType.CIRCLE)
//                .size(4d);
//        series3.tooltip()
//                .position("right")
//                .anchor(Anchor.LEFT_CENTER)
//                .offsetX(5d)
//                .offsetY(5d);

        cartesian.legend().enabled(true);
        cartesian.legend().fontSize(13d);
        cartesian.legend().padding(0d, 0d, 10d, 0d);

        anyChartView.setChart(cartesian);
    }

    private class CustomDataEntry extends ValueDataEntry {

        CustomDataEntry(String x, Number bb) {
            super(x, bb);
//            setValue("value2", tb);
        }

    }

}
