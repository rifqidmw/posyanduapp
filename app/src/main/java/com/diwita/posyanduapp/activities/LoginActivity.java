package com.diwita.posyanduapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.adapters.AnakAdapter;
import com.diwita.posyanduapp.model.DaftarAnakModel;
import com.diwita.posyanduapp.model.TokenModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    private String user;
    private String pass;
    private TextView result;
    private Button btnLogin;
    private EditText etEmail;
    private EditText etPassword;
    private ProgressBar progressBar;
    private FirebaseAuth auth;
    private DatabaseReference mDatabaseToken, mDatabaseAnak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FirebaseApp.initializeApp(this);
        auth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressBar);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                login(email, password);
            }
        });
    }

    public void login(String email, String password) {
        if (TextUtils.isEmpty(email)){
            etEmail.setError("Email harus diisi !!");
            return;
        }
        if (TextUtils.isEmpty(password)){
            etPassword.setError("Password harus diisi !!");
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (!task.isSuccessful()){
                            Toast.makeText(LoginActivity.this, "Email atau Password salah !!!", Toast.LENGTH_SHORT).show();
                        } else {
                            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                                @Override
                                public void onSuccess(InstanceIdResult instanceIdResult) {
                                    final String deviceToken = instanceIdResult.getToken();

                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                    String email = user.getEmail();

//                                    TokenModel tokenModel = new TokenModel();
//                                    tokenModel.setToken(deviceToken);
//                                    tokenModel.setUser(email);
//
//                                    mDatabaseToken = FirebaseDatabase.getInstance().getReference("token");
//                                    String id = mDatabaseToken.push().getKey();
//                                    mDatabaseToken.child(deviceToken).setValue(tokenModel);

                                    mDatabaseAnak = FirebaseDatabase.getInstance().getReference().child("daftar anak").getRef();
                                    Query query = mDatabaseAnak.orderByChild("user").equalTo(email);
                                    query.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                                                DaftarAnakModel daftarAnakModel = dataSnapshot1.getValue(DaftarAnakModel.class);
                                                DatabaseReference mDatabaseUp = FirebaseDatabase.getInstance().getReference("daftar anak").child(daftarAnakModel.getKey());
                                                mDatabaseUp.child("token").setValue(deviceToken);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            });

                            Toast.makeText(LoginActivity.this, "Login berhasil !!", Toast.LENGTH_SHORT).show();
                            Intent login = new Intent(LoginActivity.this, MenuActivity.class);
                            startActivity(login);
                        }
                    }
                });
    }
}
