package com.diwita.posyanduapp.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.diwita.posyanduapp.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.simplymadeapps.quickperiodicjobscheduler.QuickPeriodicJobScheduler;

public class MainActivity extends AppCompatActivity {
    private Button btnDaftar;
    private Button btnLogin;
    private Context mContext;

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        btnDaftar = (Button)findViewById(R.id.btnDaftar);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, LoginActivity.class));
            }
        });
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, DaftarActivity.class));
            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference("token");

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String deviceToken = instanceIdResult.getToken();

                Log.d("token", deviceToken);

            }
        });

        String key = "1";
        Boolean firstTime = getPreferences(MODE_PRIVATE).getBoolean(key, true);

        if (firstTime){
//            getPreferences(MODE_PRIVATE).edit().putBoolean(key, false).apply();
//            final String id = mDatabase.push().getKey();
//
//            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
//                @Override
//                public void onSuccess(InstanceIdResult instanceIdResult) {
//                    String deviceToken = instanceIdResult.getToken();
//
//                    mDatabase.child(id).setValue(deviceToken);
//                    Log.d("FIRST TIME", "true");
//
//                }
//            });
            int seconds = 1000;
            int minutes = seconds*60;
            int hours = minutes*60;
            int day = hours*24;
            int week = day*7;
            int month = day*30;
            QuickPeriodicJobScheduler jobScheduler = new QuickPeriodicJobScheduler(this);
            jobScheduler.start(1, 5*minutes);
            jobScheduler.start(2, minutes);
            jobScheduler.start(3, hours);
        }

//        String JOB = "POSYANDU JOB DISPATCHER";
//        FirebaseJobDispatcher jobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job job = jobDispatcher.newJobBuilder()
//                .setService(JobDispatcherService.class)
//                .setLifetime(Lifetime.FOREVER)
//                .setRecurring(true)
//                .setTag(JOB)
//                .setTrigger(JobDispatcherUtils.periodicTrigger(5, 1))
//                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
//                .setReplaceCurrent(true)
//                .setConstraints(Constraint.ON_ANY_NETWORK)
//                .build();
//        jobDispatcher.mustSchedule(job);
//        Log.d("JOBDISPATCHER", "Job running...");
//        Toast.makeText(mContext, "Posyandu App Running ....", Toast.LENGTH_SHORT).show();
    }
}
