package com.diwita.posyanduapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.adapters.MenuAdapter;

public class MenuActivity extends AppCompatActivity {
    private RecyclerView listMenu;
    private LinearLayoutManager linearLayoutManager;
    private MenuAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        listMenu = (RecyclerView)findViewById(R.id.listMenu);

        linearLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        listMenu.setLayoutManager(linearLayoutManager);
        adapter = new MenuAdapter();
        listMenu.setAdapter(adapter);

    }
}
