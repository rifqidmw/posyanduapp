package com.diwita.posyanduapp.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.adapters.AnakAdapter;
import com.diwita.posyanduapp.model.DaftarAnakModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;

public class ProfilAnak extends AppCompatActivity {

    private FloatingActionButton fab;
    private DatabaseReference mDatabase, anakDatabase;
    private ArrayList<DaftarAnakModel> daftarAnak;
    private RecyclerView rvAnak;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private String token = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_anak);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        rvAnak = (RecyclerView) findViewById(R.id.rvAnak);

        rvAnak.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rvAnak.setLayoutManager(layoutManager);

        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("daftar anak").getRef();


        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                token = instanceIdResult.getToken();
                if (token.isEmpty()){
                    Toast.makeText(ProfilAnak.this, "Token not generated...", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(ProfilAnak.this, token, Toast.LENGTH_SHORT).show();

                    Query query = mDatabase.orderByChild("token").equalTo(token);
                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            daftarAnak = new ArrayList<>();
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                                DaftarAnakModel daftarAnakModel = dataSnapshot1.getValue(DaftarAnakModel.class);
                                daftarAnakModel.setKey(dataSnapshot1.getKey());
                                Log.d("tgl: ", daftarAnakModel.getTgl());
                                daftarAnak.add(daftarAnakModel);
                            }
                            adapter = new AnakAdapter(daftarAnak, getApplicationContext());
                            rvAnak.setAdapter(adapter);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });



//        anakDatabase.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfilAnak.this, DaftarAnak.class);
                startActivity(intent);
            }
        });
    }
}
