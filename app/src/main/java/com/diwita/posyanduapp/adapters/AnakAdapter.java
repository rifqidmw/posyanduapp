package com.diwita.posyanduapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.activities.DetailNotif;
import com.diwita.posyanduapp.activities.Grafik;
import com.diwita.posyanduapp.model.DaftarAnakModel;

import java.util.ArrayList;

public class AnakAdapter extends RecyclerView.Adapter<AnakAdapter.MyViewHolder> {
    private ArrayList<DaftarAnakModel> anakList;
    private Context context;

    public AnakAdapter(ArrayList<DaftarAnakModel> anakList, Context context){
        this.anakList = anakList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_anak, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.nik.setText(anakList.get(position).getNik());
        holder.nama.setText(anakList.get(position).getNama_anak());
        holder.tgl.setText(anakList.get(position).getTgl());
        holder.jk.setText(anakList.get(position).getJk());
        holder.bb.setText(anakList.get(position).getBerat_badan());
        holder.tb.setText(anakList.get(position).getTinggi_badan());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Grafik.class);
                intent.putExtra("NIK", anakList.get(position).getNik());
                intent.putExtra("NAMA", anakList.get(position).getNama_anak());
                intent.putExtra("TGL_LAHIR", anakList.get(position).getTgl());
                intent.putExtra("JK", anakList.get(position).getJk());
                intent.putExtra("BB", anakList.get(position).getBerat_badan());
                intent.putExtra("BB_HEPATITIS1", anakList.get(position).getBbHepatitis1());
                intent.putExtra("BB_HEPATITIS2", anakList.get(position).getBbHepatitis2());
                intent.putExtra("BB_DPT", anakList.get(position).getBbDpt());
                intent.putExtra("BB_POLIO", anakList.get(position).getBbPolio());
                intent.putExtra("BB_HIB", anakList.get(position).getBbHib());
                intent.putExtra("BB_HEPATITIS3", anakList.get(position).getBbHepatitis3());
                intent.putExtra("BB_BGC", anakList.get(position).getBbBcg());
                intent.putExtra("BB_HEPATITIS4", anakList.get(position).getBbHepatitis4());
                intent.putExtra("BB_CAMPAK", anakList.get(position).getBbCampak());
                intent.putExtra("BB_MMR", anakList.get(position).getBbMmr());
                intent.putExtra("TOKEN", anakList.get(position).getToken());
                intent.putExtra("KEY", anakList.get(position).getKey());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return anakList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nik, nama, tgl, jk, bb, tb;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            nik = (TextView) itemView.findViewById(R.id.tvNIK);
            nama = (TextView) itemView.findViewById(R.id.tvNama);
            tgl = (TextView) itemView.findViewById(R.id.tvTgl);
            jk = (TextView) itemView.findViewById(R.id.tvJK);
            bb = (TextView) itemView.findViewById(R.id.tvBB);
            tb = (TextView) itemView.findViewById(R.id.tvTB);
        }
    }
}
