package com.diwita.posyanduapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.activities.DaftarImunisasi;
import com.diwita.posyanduapp.activities.DetailNotif;
import com.diwita.posyanduapp.activities.Frame1;
import com.diwita.posyanduapp.activities.Grafik;
import com.diwita.posyanduapp.activities.MenuActivity;
import com.diwita.posyanduapp.activities.ProfilAnak;

/**
 * Created by Asep Ahmad S on 3/31/2018.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_menu, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if(position==0){
            holder.frame.setBackgroundColor(Color.parseColor("#F3E809"));
            holder.tv_menu.setText("Daftar Anak");
        }else if(position==1){
            holder.frame.setBackgroundColor(Color.parseColor("#278FC5"));
            holder.tv_menu.setText("Daftar Notifikasi");
        }else if(position==2){
            holder.frame.setBackgroundColor(Color.parseColor("#6D4E89"));
            holder.tv_menu.setText("Chart");
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout frame;
        TextView tv_menu;
        Context context;
        public ViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            frame = itemView.findViewById(R.id.frameMenu);
            tv_menu = itemView.findViewById(R.id.tv_menu);
            frame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    switch (getAdapterPosition()) {
                        case 0:
                            intent = new Intent(context, ProfilAnak.class);
                            break;
                        case 1:
                            intent = new Intent(context, DetailNotif.class);
                            break;
                        case 2:
                            intent = new Intent(context, Grafik.class);
                            break;
                    }
                    context.startActivity(intent);
                }
            });
        }
    }
}
