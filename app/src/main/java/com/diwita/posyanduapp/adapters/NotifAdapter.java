package com.diwita.posyanduapp.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.diwita.posyanduapp.R;
import com.diwita.posyanduapp.model.DaftarAnakModel;
import com.diwita.posyanduapp.model.DaftarListNotifikasi;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class NotifAdapter extends RecyclerView.Adapter<NotifAdapter.MyViewHolder>  {
    private List<DaftarListNotifikasi> anakList;
    private Context context;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    public NotifAdapter(List<DaftarListNotifikasi> anakList, Context context) {
        this.anakList = anakList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_notification, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.namaNotif.setText(anakList.get(position).getNama());
        holder.umurNotif.setText(anakList.get(position).getUmur());
        holder.imunisasiNotif.setText(anakList.get(position).getImunisasi());
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new AlertDialog.Builder(context);
                dialogView = inflater.inflate(R.layout.detail_notifikasi, null);
                dialog.setView(dialogView);
                dialog.setCancelable(true);
                dialog.setTitle("UPDATE");

                final TextView tvNama = (TextView) dialogView.findViewById(R.id.tvNamaDetail);
                final TextView tvUmur = (TextView) dialogView.findViewById(R.id.tvUmurDetail);
                final TextView tvImunisasi = (TextView) dialogView.findViewById(R.id.tvImunisasiDetail);
                final EditText etBB = (EditText) dialogView.findViewById(R.id.etBB);

                tvNama.setText(anakList.get(position).getNama());
                tvUmur.setText(anakList.get(position).getUmur());
                tvImunisasi.setText(anakList.get(position).getImunisasi());

                dialog.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String child = "";
                        String nama = anakList.get(position).getNama();
                        String umur = anakList.get(position).getUmur();
                        String imunisasi = anakList.get(position).getImunisasi();
                        String bb = etBB.getText().toString();
                        String key = anakList.get(position).getKey();
                        if (umur.equals("1 Hari")){
                            child = "bbHepatitis1";
                        } else if (umur.equals("6 Minggu")){
                            child = "bbDpt";
                        } else if (umur.equals("2 Bulan") && imunisasi.equals("Imunisasi Hepatitis")){
                            child = "bbHepatitis2";
                        } else if (umur.equals("3 Bulan") && imunisasi.equals("Imunisasi Hepatitis")){
                            child = "bbHepatitis3";
                        } else if (umur.equals("4 Bulan") && imunisasi.equals("Imunisasi Hepatitis")){
                            child = "bbHepatitis4";
                        } else if (umur.equals("2 Bulan") && imunisasi.equals("Imunisasi Polio")){
                            child = "bbPolio";
                        } else if (umur.equals("2 Bulan") && imunisasi.equals("Imunisasi HIB")){
                            child = "bbHib";
                        } else if (umur.equals("3 Bulan") && imunisasi.equals("Imunisasi BCG")){
                            child = "bbBcg";
                        } else if (umur.equals("9 Bulan")){
                            child = "bbCampak";
                        } else if (umur.equals("12 Bulan")){
                            child = "bbMmr";
                        }
                        DatabaseReference mDatabaseUp = FirebaseDatabase.getInstance().getReference("daftar anak").child(key);
                        mDatabaseUp.child(child).setValue(bb);

                        DatabaseReference mDatabaseDel = FirebaseDatabase.getInstance().getReference("list notifikasi");
                        mDatabaseDel.child(key).removeValue();
                    }
                });
                dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return anakList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView namaNotif, umurNotif, imunisasiNotif;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            namaNotif = (TextView) itemView.findViewById(R.id.tvNamaNotif);
            umurNotif = (TextView) itemView.findViewById(R.id.tvUmurNotif);
            imunisasiNotif = (TextView) itemView.findViewById(R.id.tvImunisasiNotif);
        }
    }
}
