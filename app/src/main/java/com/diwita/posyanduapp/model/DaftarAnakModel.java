package com.diwita.posyanduapp.model;

public class DaftarAnakModel {
    private String key;
    private String nik;
    private String nama_anak;
    private String jk;
    private String tgl;
    private String berat_badan;
    private String tinggi_badan;
    private String token;
    private String user;
    private String bbHepatitis1;
    private String bbHepatitis2;
    private String bbDpt;
    private String bbPolio;
    private String bbHib;
    private String bbHepatitis3;
    private String bbBcg;
    private String bbHepatitis4;
    private String bbCampak;
    private String bbMmr;

    public DaftarAnakModel(){

    }

    public DaftarAnakModel(String key, String nik, String nama_anak, String jk, String tgl, String berat_badan, String tinggi_badan, String token, String user, String bbHepatitis1, String bbHepatitis2, String bbDpt, String bbPolio, String bbHib, String bbHepatitis3, String bbBcg, String bbHepatitis4, String bbCampak, String bbMmr) {
        this.key = key;
        this.nik = nik;
        this.nama_anak = nama_anak;
        this.jk = jk;
        this.tgl = tgl;
        this.berat_badan = berat_badan;
        this.tinggi_badan = tinggi_badan;
        this.token = token;
        this.user = user;
        this.bbHepatitis1 = bbHepatitis1;
        this.bbHepatitis2 = bbHepatitis2;
        this.bbDpt = bbDpt;
        this.bbPolio = bbPolio;
        this.bbHib = bbHib;
        this.bbHepatitis3 = bbHepatitis3;
        this.bbBcg = bbBcg;
        this.bbHepatitis4 = bbHepatitis4;
        this.bbCampak = bbCampak;
        this.bbMmr = bbMmr;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama_anak() {
        return nama_anak;
    }

    public void setNama_anak(String nama_anak) {
        this.nama_anak = nama_anak;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getBerat_badan() {
        return berat_badan;
    }

    public void setBerat_badan(String berat_badan) {
        this.berat_badan = berat_badan;
    }

    public String getTinggi_badan() {
        return tinggi_badan;
    }

    public void setTinggi_badan(String tinggi_badan) {
        this.tinggi_badan = tinggi_badan;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getBbHepatitis1() {
        return bbHepatitis1;
    }

    public void setBbHepatitis1(String bbHepatitis1) {
        this.bbHepatitis1 = bbHepatitis1;
    }

    public String getBbHepatitis2() {
        return bbHepatitis2;
    }

    public void setBbHepatitis2(String bbHepatitis2) {
        this.bbHepatitis2 = bbHepatitis2;
    }

    public String getBbDpt() {
        return bbDpt;
    }

    public void setBbDpt(String bbDpt) {
        this.bbDpt = bbDpt;
    }

    public String getBbPolio() {
        return bbPolio;
    }

    public void setBbPolio(String bbPolio) {
        this.bbPolio = bbPolio;
    }

    public String getBbHib() {
        return bbHib;
    }

    public void setBbHib(String bbHib) {
        this.bbHib = bbHib;
    }

    public String getBbHepatitis3() {
        return bbHepatitis3;
    }

    public void setBbHepatitis3(String bbHepatitis3) {
        this.bbHepatitis3 = bbHepatitis3;
    }

    public String getBbBcg() {
        return bbBcg;
    }

    public void setBbBcg(String bbBcg) {
        this.bbBcg = bbBcg;
    }

    public String getBbHepatitis4() {
        return bbHepatitis4;
    }

    public void setBbHepatitis4(String bbHepatitis4) {
        this.bbHepatitis4 = bbHepatitis4;
    }

    public String getBbCampak() {
        return bbCampak;
    }

    public void setBbCampak(String bbCampak) {
        this.bbCampak = bbCampak;
    }

    public String getBbMmr() {
        return bbMmr;
    }

    public void setBbMmr(String bbMmr) {
        this.bbMmr = bbMmr;
    }
}
