package com.diwita.posyanduapp.model;

public class DaftarListNotifikasi {
    String user,nama, umur, imunisasi, token, key;

    public DaftarListNotifikasi(){

    }

    public DaftarListNotifikasi(String user, String nama, String umur, String imunisasi, String token, String key) {
        this.user = user;
        this.nama = nama;
        this.umur = umur;
        this.imunisasi = imunisasi;
        this.token = token;
        this.key = key;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public String getImunisasi() {
        return imunisasi;
    }

    public void setImunisasi(String imunisasi) {
        this.imunisasi = imunisasi;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
