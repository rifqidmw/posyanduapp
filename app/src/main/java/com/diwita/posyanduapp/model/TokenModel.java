package com.diwita.posyanduapp.model;

public class TokenModel {
    String token, user;

    public TokenModel() {
    }

    public TokenModel(String token, String user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
