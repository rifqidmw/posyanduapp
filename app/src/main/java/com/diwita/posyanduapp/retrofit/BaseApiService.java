package com.diwita.posyanduapp.retrofit;

import com.diwita.posyanduapp.model.ResponseNotificationModel;
import com.diwita.posyanduapp.model.ValueNotificationModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface BaseApiService {
    @POST("sendSinglePush.php")
    Call<ResponseNotificationModel> sendNotification(@Body ValueNotificationModel valueNotificationModel);
}
