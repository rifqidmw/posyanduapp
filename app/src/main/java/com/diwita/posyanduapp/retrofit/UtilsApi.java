package com.diwita.posyanduapp.retrofit;

public class UtilsApi {
    public static final String BASE_URL_API = "http://192.168.186.2/firebasenoification/";

    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
